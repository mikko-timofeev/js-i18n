'use strict';

export const paramDecode = (urlParams) => {
  const put = (obj, path, val) => {
    const stringToPath = (propertyPath) => {
      if (typeof propertyPath !== 'string') {
        return propertyPath;
      }
      const output = [];
      propertyPath
        .split('.')
        .forEach(
          (item) => {
            item
              .split(/\[([^}]+)\]/g)
              .forEach(
                (key) => {
                  if (key.length > 0) {
                    output.push(key);
                  }
                }
              );
          }
        );
      return output;
    };

    path = stringToPath(path);

    const [length] = [path.length];
    let current = obj;

    path.forEach((key, index) => {
      const isArray = key.slice(-2) === '[]';
      key = isArray ? key.slice(0, -2) : key;
      if (
        isArray &&
        Object.prototype.toString.call(current[key]) !== '[object Array]'
      ) {
        current[key] = [];
      }
      if (index === length - 1) {
        if (isArray) {
          current[key].push(val);
        } else {
          current[key] = val;
        }
      } else {
        if (!current[key]) {
          current[key] = {};
        }
        current = current[key];
      }
    });
  };

  return urlParams
    .split('&')
    .reduce((obj, item) => {
      const key = item.split('=')[0];
      if (key !== '') {
        const keyPath = decodeURIComponent(key)
          .replace(']', '')
          .split('[');
        put(obj, keyPath, decodeURIComponent(item.split('=')[1]));
      }
      return obj;
    }, {});
};

export const paramEncode = jsParams => Object
  .keys(jsParams)
  .map(
    key => [key, jsParams[key]].map(encodeURIComponent).join('=')
  )
  .join('&');

export const pageLinkOverride = (page, params, uuid = false, reload = false) => {
  // if (params.action === 'create') {
  //   uuid = '';
  // }
  if (params.action !== 'edit') {
    delete params.action;
  }
  // if (Object.entries(params).length === 0 && !uuid) {
  //   uuid = 'new';
  // }
  const validPath = `/?${
    page
  }${
    uuid ? `=${uuid}` : ''
  }${
    Object.entries(params).length > 0 ? `&${paramEncode(params)}` : ''
  }`;
  window.history.pushState({}, page, validPath);
  if (reload) {
    window.location.reload();
  }
};

export const serialize = (form) => {
  const s = [];

  if (
    typeof form === 'object' &&
    form.nodeName === 'FORM'
  ) {
    const len = form.elements.length;

    for (let i = 0; i < len; i += 1) {
      const field = form.elements[i];
      if (
        field.name &&
        !field.disabled &&
        field.type !== 'button' &&
        field.type !== 'file' &&
        field.type !== 'hidden' &&
        field.type !== 'reset' &&
        field.type !== 'submit'
      ) {
        if (field.type === 'select-multiple') {
          const l = form.elements[i].options.length;
          for (let j = 0; j < l; j += 1) {
            if (field.options[j].selected) {
              s[s.length] = `${encodeURIComponent(field.name)}=${encodeURIComponent(field.options[j].value)}`;
            }
          }
        } else if (
          (
            field.type !== 'checkbox' &&
            field.type !== 'radio'
          ) ||
          field.checked
        ) {
          s[s.length] = `${encodeURIComponent(field.name)}=${encodeURIComponent(field.value)}`;
        }
      }
    }
  }
  return s.join('&').replace(/%20/g, '+');
};

export const booleize = (val) => {
  if (val === 'true') {
    return true;
  }
  if (val === 'false') {
    return false;
  }
  return val;
};

export const deepList = (obj, onlyDeepest = true, prefix = '') => {
  const removables = [];
  return Object.entries(obj).reduce(
    (collector, [key, val]) => {
      const newKeys = [...collector, prefix ? `${prefix}.${key}` : key];
      if (Object.prototype.toString.call(val) === '[object Object]') {
        const newPrefix = prefix ? `${prefix}.${key}` : key;
        if (onlyDeepest) { // remove keys that are objects
          removables.push(newPrefix);
        }
        const otherKeys = deepList(val, onlyDeepest, newPrefix);
        return [...newKeys, ...otherKeys];
      }

      return newKeys;
    }, []
  ).filter(
    item => !removables.includes(item)
  );
};

export const deepFind = (obj, path, allowObjects = false) => {
  const paths = path.split('.');

  for (let i = 0; i < paths.length; i += 1) {
    if (
      obj === undefined ||
      obj[paths[i]] === undefined
    ) {
      return undefined;
    }
    obj = obj[paths[i]];
  }

  return (
    allowObjects ||
    typeof obj === 'string' ||
    typeof obj === 'boolean'
  ) ? obj : undefined;
};

export const deepMerge = (target, ...sources) => {
  const isObject = item => (
    item &&
    typeof item === 'object' &&
    !Array.isArray(item)
  );

  if (!sources.length) {
    return target;
  }
  const source = sources.shift();

  if (
    isObject(target) &&
    isObject(source)
  ) {
    // eslint-disable-next-line no-restricted-syntax
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) {
          Object.assign(target, { [key]: {} });
        }
        deepMerge(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: booleize(source[key]) });
      }
    }
  }

  return deepMerge(target, ...sources);
};
