#!/bin/sh
. ./.env
PORT="${PORT:-8000}"
python -m http.server $PORT --bind 127.0.0.1
